-## ChangeLog

# v0.0.4 - 01 marzo 2020
  * Se agregó tabs en home para versión móvil

# v0.0.3 - 29 febrero 2020
  * Agregar diseño base home para versión móvil
  * Se arregló problema de eslint
  * Se arregló error en .gitignore que ignoraba carpeta src

# v0.0.2 - 28 febrero 2020
  * Se agregó template de home
  * Se agregó funcionalidad básica a menú lateral

# v0.0.1 - 27 febrero 2020
  * Agregar template de menú lateral
  * Se configuró inicio de sesión para trabajar con vuex
  * Se agregó funcionalidad básica a inicio de sesión
  * Se agregó template inicio de sesión
