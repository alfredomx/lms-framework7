import Home from './views/pag-home.vue'
import menuLateral from './views/menu-lateral.vue'
import NoEncontrado from './views/pag-not-found.vue'

var routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/menu-lateral/',
    component: menuLateral
  },
  {
    path: '(.*)',
    component: NoEncontrado
  }
]

export default routes
