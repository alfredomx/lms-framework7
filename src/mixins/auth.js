import { mapGetters, mapActions } from 'vuex'

export const AuthStore = {
  computed: {
    ...mapGetters('auth', [
      'key',
      'estatus'
    ])
  },
  methods: {
    ...mapActions('auth', {
      'iniciarSesion': 'INICIAR_SESION',
      'cerrarSesion': 'CERRAR_SESION'
    })
  }
}