export default {
  install: (Vue, options) => {
    Vue.$helper = Vue.prototype.$helper = {
      arr: {
        entre: (x, a, b) => {
          let min = Math.min(a, b)
          let max = Math.max(a, b)
          return x >= min && x <= max
        }
      },
      str: {
        capitalizar: (s) => {
          if (typeof s !== 'string') { return '' }
          return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase()
        },
        formatearMoneda: (cantidad) => {
          return Number(cantidad).toLocaleString('es-MX', { style: 'currency', currency: 'MXN' })
        }
      },
      num: {
        entre: (x, a, b) => {
          return x >= Math.min(a, b) && x <= Math.max(a, b)
        },
        redondear: (n) => {
          return Math.round(Number(n) * 100 + Number.EPSILON) / 100
        }
      }
    }
  }
}
