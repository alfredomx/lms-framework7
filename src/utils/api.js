import _ from 'lodash'
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)

Vue.axios.defaults.baseURL = 'http://localhost:3000/api/'
Vue.axios.defaults.headers.common['Authorization'] = `${localStorage.getItem('token')}`

const api = {
  
}

function obtenerError (err) {
  if (err.response && err.response.data && err.response.data.Message) {
    if (err.response.data.Message === 'Authorization has been denied for this request.') {
      window.localStorage.removeItem('token')
      window.location = '/'
    } else {
      return err.response.data.Message
    }
  } else {
    return 'Error al intentar conectarse al servidor.'
  }
}

export default {
  install: (Vue, options) => {
    Vue.$api = Vue.prototype.$api = api
  }
}
