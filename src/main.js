import Vue from 'vue'
import Framework7 from 'framework7/framework7-lite.esm.bundle.js'
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js'
import VueAxios from 'vue-axios'
import axios from 'axios'
import lodash from 'lodash'
import api from '@/utils/api'
import store from './store'
import persistentState from 'vue-persistent-state'
import Vue2TouchEvents from 'vue2-touch-events'
import { storage } from './utils/init'
import models from './models'
import helper from '@/utils/helpers'
import app from './utils/app'
import 'framework7/css/framework7.bundle.css'
import './assets/css/icons.css'
import './assets/css/app.scss'

import App from './app.vue'

Framework7.use(Framework7Vue)
Vue.use(VueAxios, axios)
Vue.use(api)
Vue.use(Vue2TouchEvents)
Vue.use(helper)
Vue.use(app)

Vue.use({
  install: (Vue, options) => {
    Vue.$evento = Vue.prototype.$evento = new Framework7.Events()
    Vue.$lodash = Vue.prototype.$lodash = lodash
    Vue.$model = Vue.prototype.$model = models
  }
})

Vue.use(persistentState, {
  storage
})

new Vue({
  el: '#app',
  store,
  render: (h) => h(App),
  components: {
    app: App
  }
})
