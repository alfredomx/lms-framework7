import 'objectmodel/src/devtool-formatter'
import { ObjectModel  } from 'objectmodel'

export const Usuario = new ObjectModel({
  nombre: String,
  correo: String,
  clave: String
}).defaultTo({
	nombre: '',
	correo: '',
	clave: ''
})
