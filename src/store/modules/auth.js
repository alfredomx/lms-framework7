const state = {
  key: '',
  estatus: '',
  error: ''
}

const getters = {
  key: state => state.key,
  estatus: state => state.estatus
}

const mutations = {
  INICIAR_SESION: (state, payload) => {
    state.estatus = 'INICIANDO_SESION'
    console.log('Iniciando sesión...')
    setTimeout(() => {
      state.estatus = "AUTENTICADO"
      state.key = '1234567890'
      console.log('Usuario autenticado...')
    }, 2000)
  },
  CERRAR_SESION: (state, payload) => {
    state.estatus = 'SESION_FINALIZADA'
    state.key = ''
    console.log('Sesión finalizada...')
  }
}

const actions = {
  INICIAR_SESION: ({ commit, state, rootState }) => {
    commit('INICIAR_SESION')
  },
  CERRAR_SESION: ({ commit, state, rootState }) => {
    commit('CERRAR_SESION')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
