import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/store/modules/auth'
import template from './template'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    template,
    auth
  }
})
